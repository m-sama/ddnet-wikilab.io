---
title: "Hook"
weight: 10
---

The Hook allows you to maneuver around maps. It is always available and essential to almost every ddnet map.
With it, you can: 

- hook certain blocks to swing around
- hook other tees to pull them towards you


Use the hook by holding down secondary fire. Release it by letting go of secondary fire.

How it functions:

- the hook moves in a straight line until it collides with either a tee or a wall
- if the hook collides with nothing, it will stop at its maximum range
- hooking is not instant, it does have a travel time
- hooks do not nullify the former velocity of a tee, its just another factor that influences it
- thanks to how thin it is, it can fit through diagonal gaps between blocks

### Block Types

There are 3 types of solid blocks:

- Hooking unhookable blocks will cancel your hook
- Hooking a hookable block will anchor your hook into place, until you release it
- Hookthrough blocks will not affect your hook, it will pass right through them

When your hook is active (anchored to a hookable block), your tee will be pulled towards the anchor point.  
There is no timer when hooking a block, you can release it at any time.

### Hooking Tees

Hooking another tee will pull the tee towards you.  
Your tee will also be slightly pulled towards the other tee.

Keep in mind that, due to friction, pulling tees is way slower while they are standing on the floor.
In contrast to hooking a block, a hook on another tee only lasts 1.25 sec. Once the timer runs out, the hook will be reset.

#### Pull limit of the hook

You will not pulled all the way to the anchor point.
Instead, when your tee enters a specific radius to the achor point you will not be affected by the hook until you get out of it again.
This is especially significant when trying to move along the ceiling. Try to rehook the ceiling when your tee is directly below it, to stick tighter to the it.

#### Hook Hitbox of Tees

The hitbox of tees for the hook is actually larger then it is visually represented. While a tee looks about the same size as a block and mostly also interacts that way, its hitbox for the hook is much more generous.
Due to this large hitbox it is possible to hook a tee through a gap between blocks without having to do a hook with surgical precision.
To see how big the actual hit box of tees are, you can turn on 'fat skins' in Settings -> Tee -> Fat Skins(DDFat).

#### Weak Hook - Strong Hook

When hooking other tees, you won't always pull them with the same strength.
There are 2 different strength modes your hook can have, called 'weak hook' and 'strong hook'.  
With every tee on the server you individually have either weak, or strong hook on them.
Who of you 2 has strong hook on the other is determined by who spawned last.
The tee that spawned last has weak hook and the one who already lived longer has strong hook.

Weak and Strong hook are especially noticeble in parts where you hook the other tee along the floor and during hammerfly.

{{% vid hook-nomove %}}
{{% vid hook-move %}}

### Advanced Behaviour

- the hook has a maximum range. Once the hook expanded to its maximum range, it will reset. Note that if you move away from the hook, that maximum range is reached faster and you won't hook as far. However, you can't hook further by moving in the same direction
- the hook does not start directly at the border of your tee you see. Instead, it starts a bit further out, which is why its easier too hook through a gap between blocks when you are near the gap yourself
