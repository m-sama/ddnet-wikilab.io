---
title: "Speedup Layer"
explain: true
weight: 60
---

#### Tile representations of the 'speedup' layer in the editor.

Contains the speedup tile.
Necessary due to its unique properties.

{{% explain file="static/explain/speedup.svg" %}}
